import {Component, OnInit} from '@angular/core';
import {interval, of, race, timer} from 'rxjs';
import {mapTo, min} from 'rxjs/operators';

const obs1 = interval(2000).pipe(mapTo('Sonic'));
const obs2 = interval(3000).pipe(mapTo('Bugatti Chiron'));
const obs3 = interval(1000).pipe(mapTo('Julians kat'));


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  timermessage: string;
  minmessage: number;
  racemessage: string;


  // deze functie checkt de tijd en verandert de tijd om de 1 seconden
  checktime(): any {
    const numbers = timer(0, 1000);

    numbers.subscribe(x => this.timermessage = 'Aantal seconden in je leven die je hebt verspilt met staren naar deze pagina' + ' ' + x);
  }

  // Checkt de laagste waarde
  min(): void {
    of(5, 4, 7, 2, 8).pipe(
      min(),
    )
      .subscribe(x => this.minmessage = x); // -> 2
  }

  // Checkt wie de sneslte racer is
  checkrace(): any {
    race(obs3, obs1, obs2).subscribe(winner => this.racemessage = winner);
  }

  constructor() {
  }

  ngOnInit() {
    this.checktime();
    this.min();
  }

}
